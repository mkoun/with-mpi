#include "average.h"
#include <stdio.h>
#include <mpi.h>

int main(int argc, char **argv)
{
	if(argc != 2) {
		fprintf(stderr, "Usage: average num_elements_per_proc\n");
		exit(1);
	}
	int numprocele = atoi(argv[1]);
	srand(time(NULL));

	MPI_Init(NULL, NULL);

	int rank, world;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world);

	float *rand_nums = NULL;
	if(rank == 0)
		rand_nums = create_rand_nums(numprocele * world);

	// For each proc, create a buffer
	float *sub_rand_nums = (float *)malloc(sizeof(float) * numprocele);
	assert(sub_rand_nums != NULL);
	MPI_Scatter(rand_nums, numprocele, MPI_FLOAT, sub_rand_nums, numprocele, MPI_FLOAT, 0, MPI_COMM_WORLD);
	float sub_avg = computeavg(sub_rand_nums, numprocele);

	// For all proc
	float *sub_avgs = (float *)malloc(sizeof(float) * world);
	assert(sub_avgs != NULL);
	MPI_Allgather(&sub_avg, 1, MPI_FLOAT, sub_avgs, 1, MPI_FLOAT, MPI_COMM_WORLD);

	float avg = computeavg(sub_avgs, world);
	printf("Avg of all elements is %f\n", avg);

	if(rank == 0) {
		free(rand_nums);
	}
	free(sub_avgs);
	free(sub_rand_nums);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	return 0;
}
