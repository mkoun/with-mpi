#include "parrank.h"
#include <stdlib.h>
#include <string.h>

int compare_float(const void *a, const void *b)
{
	commranknum *ap = (commranknum *)a;
	commranknum *bp = (commranknum *)b;
	if(ap->num.f > bp->num.f) return -1;
	else if(ap->num.f < bp->num.f) return 1;
	else return 0;
}

int compare_int(const void *a, const void *b)
{
	commranknum *ap = (commranknum *)a;
	commranknum *bp = (commranknum *)b;
	if(ap->num.i > bp->num.i) return -1;
	else if(ap->num.i < bp->num.i) return 1;
	else return 0;
}

int *getranks(void *gatherednums, mpistuff *ptr)
{
	int cnt = ptr->world;
	commranknum *ranknums = malloc(cnt * sizeof(commranknum));
	int i;
	for(i = 0; i < cnt; i++) {
		ranknums[i].comm_rank = i;
		memcpy(&(ranknums[i].num), gatherednums + (i * ptr->typesize), ptr->typesize);
	}
	if(ptr->type == MPI_FLOAT)
		qsort(ranknums, cnt, sizeof(commranknum), &compare_float);
	else
		qsort(ranknums, cnt, sizeof(commranknum), &compare_int);

	int *ranks = (int *)malloc(sizeof(int) * cnt);
	for(i=0; i < cnt; i++)
		ranks[ranknums[i].comm_rank] = i;
	free(ranknums);

	return ranks;
}

void parrank(void *senddata, void *recvdata, mpistuff *ptr)
{
	printf("[%d/%d]\n", ptr->rank, ptr->world);

	// gather nums to root 0

	void *gatherednums;
	if(ptr->rank == 0)
		gatherednums = malloc(ptr->typesize * ptr->world);
	MPI_Gather(senddata, 1, ptr->type, gatherednums, 1,  ptr->type, 0, ptr->comm);

	int *finalranks = NULL;
	if(ptr->rank == 0)
		finalranks = getranks(gatherednums, ptr);
	MPI_Scatter(finalranks, 1, MPI_INT, recvdata, 1, MPI_INT, 0, ptr->comm);

	if(ptr->rank == 0) {
		free(gatherednums);
		free(finalranks);
	}
}
