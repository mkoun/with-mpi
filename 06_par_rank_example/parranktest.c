#include "parrank.h"
#include <time.h>
#include <stdlib.h>

void init(mpistuff *stuff)
{
	MPI_Comm_rank(MPI_COMM_WORLD, &stuff->rank);
	MPI_Comm_size(MPI_COMM_WORLD, &stuff->world);
	stuff->comm = MPI_COMM_WORLD;
	stuff->type = MPI_FLOAT;
	MPI_Type_size(stuff->type, &stuff->typesize);
}

int main()
{
	MPI_Init(NULL, NULL);

	mpistuff stuff;
	init(&stuff);
	srand(time(NULL) * stuff.rank);
	float rand_num = rand() / (float)RAND_MAX;

	int pr; // parallel rank
	parrank(&rand_num, &pr, &stuff);
	printf("Rank for %f on proc %d - %d\n", rand_num, stuff.rank, pr);

	MPI_Barrier(stuff.comm);
	MPI_Finalize();
	return 0;
}

