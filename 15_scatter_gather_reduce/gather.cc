#include "average.h"
#include <boost/mpi.hpp>
#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;
using namespace boost;

#define ROOTPROC 0

int main(int argc, char **argv)
{
	mpi::environment env(argc, argv);
	mpi::communicator world;

	srand(time(0) + world.rank());
	int randnum = rand();

	if(world.rank() == 0) {
		vector<int> nums;
		mpi::gather(world, randnum, nums, ROOTPROC); // receive from all
		print_vec<int>(nums);
	}
	else {
		mpi::gather(world, randnum, ROOTPROC); // send to root
	}
	return 0;
}
