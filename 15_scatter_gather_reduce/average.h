#ifndef AVERAGE_H
#define AVERAGE_H

#include <vector>
#include <iostream>
using namespace std;

template<class T>
void create_rand_nums(vector<T> &vec, size_t n)
{
	while(n--)
		vec.push_back(rand() / (T)RAND_MAX);
}

template<class T>
void print_vec(vector<T> &vec)
{
	for(auto i = vec.begin(); i != vec.end(); ++i)
		cout << *i << endl;
}

template<class T>
float compute_avg(vector<T> &vec)
{
	T sum = 0;
	for(auto i = vec.begin(); i != vec.end(); ++i)
		sum += *i;
	return sum / vec.size();
}

#endif // ifndef AVERAGE_H
