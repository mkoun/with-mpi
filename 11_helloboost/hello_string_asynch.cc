/*
 * Boost's asynchronous data exchange
 * with isend() and irecv()
 */

#include <boost/mpi.hpp>
#include <boost/serialization/string.hpp>
#include <string>
#include <iostream>
using namespace boost;
using namespace std;

int main(int argc, char **argv)
{
	mpi::environment env{argc, argv};
	mpi::communicator world;
	string sendstr = "Hello Bunny";

	if(world.rank() == 0) {
		string strs[3];
		mpi::request reqs[3];
		reqs[0] = world.irecv(1, 16, strs[0]);
		reqs[1] = world.irecv(2, 16, strs[1]);
		reqs[2] = world.irecv(3, 16, strs[2]);
		mpi::wait_all(reqs, reqs + 3);
		cout << strs[0] << "; " << strs[1] << "; " << strs[2] << endl;
	}
	else if(world.rank() == 1) {
		sendstr += " 1";
		world.send(0, 16, sendstr);
	}
	else if(world.rank() == 2) {
		sendstr += " 2";
		world.send(0, 16, sendstr);
	}
	else if(world.rank() == 3) {
		sendstr += " 3";
		world.send(0, 16, sendstr);
	}

	return 0;
}

