#include <boost/mpi.hpp>
#include <iostream>
#include <string.h>
using namespace boost;
using namespace std;

#define MAXNUM 100

int main(int argc, char **argv)
{
	mpi::environment env{argc, argv};
	mpi::communicator world;
	char str[MAXNUM];

	if(world.rank() == 0) {
		mpi::status stat;
		stat = world.recv(mpi::any_source, 16, str, MAXNUM); // MAXNUM gives buffer size (runtime err without it)
		cout << "From proc " << stat.source() << ": " << str  << " with tag " << stat.tag() << endl;
	}
	else {
		strcpy(str, "Hello Bunny"); // implicitly passing '\0' at the end
		world.send(0, 16, str, (unsigned)strlen(str));
	}
	return 0;
}
