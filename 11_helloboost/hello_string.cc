#include <boost/mpi.hpp>
#include <boost/serialization/string.hpp>
#include <string>
#include <iostream>
using namespace boost;
using namespace std;

int main(int argc, char **argv)
{
	mpi::environment env{argc, argv};
	mpi::communicator world;
	string str;

	if(world.rank() == 0) {
		mpi::status stat;
		stat = world.recv(mpi::any_source, 16, str); // with string (not like char arr) doesn't need buffer bound
		cout << "From proc " << stat.source() << ": " << str << " m_count: " << stat.m_count << endl;
	}
	else {
		str = "Hello Bunny";
		world.send(0, 16, str);
	}
	return 0;
}
