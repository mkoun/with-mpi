#ifndef RANDWALK_H
#define RANDWALK_H

#include <vector>

typedef struct {
	int location;
	int num_steps_left_in_walk;
} Walker;

void decompose_domain(int, int, int, int &, int &);
void initialize_walkers(int, int, int, int, std::vector<Walker> &);
void walk(Walker &, int, int, int, std::vector<Walker> &);
void send_walkers(std::vector<Walker> &, int, int);
void recv_walkers(std::vector<Walker> &, int, int);

#endif // ifndef RANDWALK_H
