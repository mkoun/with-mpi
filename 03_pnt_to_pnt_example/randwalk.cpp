#include "randwalk.h"
#include <mpi.h>
#include <iostream>
#include <cstdlib>
#include <time.h>
using namespace std;

int main(int argc, char **argv)
{
	int domain_size;
	int max_walk_size;
	int num_walkers_per_proc;

	if(argc < 4) {
		cerr << "Usage: randwalk domain_size max_walk_size num_walkers_per_proc" << endl;
		exit(1);
	}

	domain_size = atoi(argv[1]);
	max_walk_size = atoi(argv[2]);
	num_walkers_per_proc = atoi(argv[3]);

	MPI_Init(NULL, NULL);

	int num_proc, rank_proc;
	MPI_Comm_size(MPI_COMM_WORLD, &num_proc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank_proc);
	srand(time(NULL) * rank_proc);

	int subdomain_start, subdomain_size;
	vector<Walker> incoming_walkers;
	vector<Walker> outgoing_walkers;

	decompose_domain(domain_size, rank_proc, num_proc, subdomain_start, subdomain_size);
	initialize_walkers(num_walkers_per_proc, max_walk_size, subdomain_start, subdomain_size, incoming_walkers);
	cout << "Proc " << rank_proc << " initiated " << num_walkers_per_proc << " walkers in subdomain "
		<< subdomain_start << " - " << subdomain_start + subdomain_size - 1 << endl;

	for(int i=0; i < incoming_walkers.size(); i++) {
		walk(incoming_walkers[i], subdomain_start, subdomain_size, domain_size, outgoing_walkers);
	}
	cout << "Proc " << rank_proc << " sending " << outgoing_walkers.size() << " outgoing walkers to proc "
		<< (rank_proc + 1) % num_proc << endl;
	if(rank_proc % 2 == 0) {
		send_walkers(outgoing_walkers, rank_proc, num_proc);
		recv_walkers(incoming_walkers, rank_proc, num_proc);
	} else {
		recv_walkers(incoming_walkers, rank_proc, num_proc);
		send_walkers(outgoing_walkers, rank_proc, num_proc);
	}
	cout << "Proc " << rank_proc << " received " << incoming_walkers.size() << " incoming walkers " << endl;

	cout << "PROC " << rank_proc << " DONE" << endl;
	MPI_Finalize();
	return 0;
}

void decompose_domain(int domain_size, int rank, int num_proc, int &subdomain_start, int &subdomain_size)
{
	if(num_proc > domain_size)
		MPI_Abort(MPI_COMM_WORLD, 1);
	subdomain_start = domain_size / num_proc * rank;
	subdomain_size = domain_size / num_proc;
	if(rank == num_proc - 1)
		subdomain_size += domain_size % num_proc;
}

void initialize_walkers(int num_walkers_per_proc, int max_walk_size, int subdomain_start, int subdomain_size, vector<Walker> &in_vw)
{
	Walker w;
	for(int i=0; i < num_walkers_per_proc; i++) {
		w.location = subdomain_start;
		w.num_steps_left_in_walk = (rand() / (float)RAND_MAX) * max_walk_size;
		in_vw.push_back(w);
	}
}

void walk(Walker &w, int subdomain_start, int subdomain_size, int domain_size, vector<Walker> &out_vw)
{
	while(w.num_steps_left_in_walk > 0) {
		if(w.location == subdomain_start + subdomain_size) {
			if(w.location == domain_size) {
				w.location = 0;
			}
			out_vw.push_back(w);
			break;
		} else {
			w.num_steps_left_in_walk--;
			w.location++;
		}
	}
}

void send_walkers(vector<Walker> &vw, int rank, int num_proc)
{
	MPI_Send((void *)vw.data(), vw.size() * sizeof(Walker), MPI_BYTE, (rank + 1) % num_proc, 0, MPI_COMM_WORLD);
	vw.clear();
}

void recv_walkers(vector<Walker> &vw, int rank, int num_proc)
{
	MPI_Status stat;
	int incoming_rank = (rank == 0) ? num_proc-1 : rank-1;
	MPI_Probe(incoming_rank, 0, MPI_COMM_WORLD, &stat);
	int incoming_size;
	MPI_Get_count(&stat, MPI_BYTE, &incoming_size);
	vw.resize(incoming_size / sizeof(Walker));
	MPI_Recv((void *)vw.data(), incoming_size, MPI_BYTE, incoming_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

