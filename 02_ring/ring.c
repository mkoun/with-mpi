#include <stdio.h>
#include <mpi.h>

#define TAG 0

int main(int argc, char **argv) {
	MPI_Init(NULL, NULL);

	int worldsize, rank;
	MPI_Comm_size(MPI_COMM_WORLD, &worldsize);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int prevrank = (rank == 0) ? (worldsize - 1) : (rank - 1);
	int nextrank = (rank + 1) % worldsize;
	int token;

	if(worldsize < 2)
		MPI_Abort(MPI_COMM_WORLD, 1);

	token = rank;
	MPI_Send(&token, 1, MPI_INT, nextrank, TAG, MPI_COMM_WORLD);
	printf("SENT from %d\n", token);

	MPI_Recv(&token, 1, MPI_INT, prevrank, TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	printf("RECVed at %d\n", token);

	MPI_Finalize();
}

