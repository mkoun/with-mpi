#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

int num_proc, rank;

void My_Bcast(void *data, int cnt, MPI_Datatype type, int root, MPI_Comm comm)
{
	if(rank == root) {
		int i;
		for(i=0; i < num_proc; i++) {
			if(i != rank)
				MPI_Send(data, cnt, type, i, 0, comm);
		}
	} else {
		MPI_Recv(data, cnt, type, root, 0, comm, MPI_STATUS_IGNORE);
	}
}

int main(int argc, char **argv) {
	if(argc != 3) {
		fprintf(stderr, "Usage: eval num_elements num_trials\n");
		exit(1);
	}
	int num_elements = atoi(argv[1]);
	int num_trials = atoi(argv[2]);

	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &num_proc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	double time_mybcast = 0.0;
	double time_mpibcast = 0.0;
	int *data = (int *)malloc(sizeof(int) * num_elements);
	assert(data != NULL);

	int i;
	for(i = 0; i < num_trials; i++) {
		// my bcast
		MPI_Barrier(MPI_COMM_WORLD); // synchromize before starting time
		time_mybcast -= MPI_Wtime();
		My_Bcast(data, num_elements, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD); // synchromize before getting final time
		time_mybcast += MPI_Wtime();

		// mpi bcast
		MPI_Barrier(MPI_COMM_WORLD);
		time_mpibcast -= MPI_Wtime();
		MPI_Bcast(data, num_elements, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
		time_mpibcast += MPI_Wtime();
	}

	if(rank == 0) {
		printf("data sz = %d, trials = %d, ", num_elements * (int)sizeof(int), num_trials);
		printf("avg my_bcast time = %lf, avg mpi_bcast time = %lf\n", time_mybcast/num_trials, time_mpibcast/num_trials);
	}

	free(data);
	MPI_Finalize();
}

