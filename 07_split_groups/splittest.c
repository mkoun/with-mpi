#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char **argv)
{
	MPI_Init(NULL, NULL);
	
	int world, rank;
	MPI_Comm_size(MPI_COMM_WORLD, &world);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int color = rank / 4;
	MPI_Comm rowcomm;
	MPI_Comm_split(MPI_COMM_WORLD, color, rank, &rowcomm);

	int rowrank, rowworld;
	MPI_Comm_size(rowcomm, &rowworld);
	MPI_Comm_rank(rowcomm, &rowrank);

	printf("World rank/size = %d/%d\tRow rank/size = %d/%d\n", rank, world, rowrank, rowworld);

	MPI_Comm_free(&rowcomm);
	MPI_Finalize();
	return 0;
}

