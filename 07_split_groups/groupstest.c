// groupstest.c
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char **argv)
{
	MPI_Init(NULL, NULL);
	
	MPI_Group world_g;
	int world, rank;
	MPI_Comm_size(MPI_COMM_WORLD, &world);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_group(MPI_COMM_WORLD, &world_g);

	MPI_Group new_g;
	MPI_Comm new_comm;
	int new_rank, new_world;

	int n = 7;
	const int ranks[] = {1,2,3,5,7,11,13};
	MPI_Group_incl(world_g, n, ranks, &new_g);
	MPI_Comm_create_group(MPI_COMM_WORLD, new_g, 0, &new_comm);
	new_rank = new_world = -1;
	if(MPI_COMM_NULL != new_comm) {
		MPI_Comm_rank(new_comm, &new_rank);
		MPI_Comm_size(new_comm, &new_world);
	}

	printf("World rank/size = %d/%d\t New rank/size = %d/%d\n", rank, world, new_rank, new_world);

	MPI_Group_free(&world_g);
	MPI_Group_free(&new_g);
	MPI_Finalize();
	return 0;
}

